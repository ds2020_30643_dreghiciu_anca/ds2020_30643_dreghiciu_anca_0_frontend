import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import DoctorForm from "./components/doctor-form-add";
import DoctorFormUpdate from "./components/doctor-form-update";

import * as API_DOCTOR from "./api/doctor-api"
import DoctorTable from "./components/doctor-table";



class DoctorContainer extends React.Component {

    constructor(props) {
        super(props);

        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selectedUpdate: false,
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
        this.handleDeleteButton = this.handleDeleteButton.bind(this);
        this.addButtons = this.addButtons.bind(this);
        this.updateDoctor = this.updateDoctor.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);

    }



    componentDidMount() {
        this.fetchDoctors();
    }
    reloadDelete() {
        this.setState({
           isLoaded: false
           });
        this.fetchDoctors();
    }

    handleDeleteButton = (idDoctor) => {
        console.log( "Id doctor for delete: "+ idDoctor);
        return API_DOCTOR.deleteDoctor(idDoctor, (result, status, err) =>{
            if(result !== null && status === 200) {
                this.reloadDelete();
            }
            else{
                this.setState({
                    errorStatus: status,
                    error: err
                })
            }

        });
    }

    updateDoctor = (doctor) =>
    {
        console.log("Id doctor for update: "+doctor.doctorId);
        this.setState({
            idDoctor: doctor.doctorId
        })

        console.log("stare id: "+ this.idDoctor);
        this.toggleFormUpdate(doctor);
    }

    addButtons = (data) => {
        const newData = data.map( doctor => {
        console.log("doctorrrr id add buttons: "+ doctor.doctorId);
            return{
                ...doctor,
                deleteDoctor:
                    <Button color = "danger" onClick={this.handleDeleteButton.bind(this, doctor.doctorId)}>
                        Delete
                    </Button>,
                updateDoctor:
                    <Button color="info" onClick={this.updateDoctor.bind(this, doctor)}>
                        Update
                    </Button>

            }

        });
        return newData;
    }


    fetchDoctors() {
        return API_DOCTOR.getDoctors((result, status, err) => {
        if(result !== null && status === 200) {
           let newTableData = this.addButtons(result);
           this.setState({
             tableData: newTableData,
             isLoaded: true
           })
        }
        else
            {
            this.setState({
                errorStatus: status,
                error: err
            });
            }

        });

    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleFormUpdate(idDoctor)
    {
        console.log("id doctor in toggle dar in container: "+ idDoctor);
        this.setState({selectedUpdate: !this.state.selectedUpdate});

    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchDoctors();
    }



    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Doctors Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Doctor </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <DoctorTable tableData = {this.state.tableData}/>
                            }
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>


                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Doctor: </ModalHeader>
                    <ModalBody>
                        <DoctorForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}> Update Doctor: </ModalHeader>
                    <ModalBody>
                        <DoctorFormUpdate reloadHandler={this.reload} doctorId={this.state.idDoctor}/>
                    </ModalBody>
                </Modal>
            </div>

        )

    }
}


export default DoctorContainer;
