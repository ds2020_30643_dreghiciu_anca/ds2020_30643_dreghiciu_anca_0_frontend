import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Specialization',
        accessor: 'specialization',
    },
    {
        Header: 'Birthdate',
        accessor: 'birthdate',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Header: 'Delete',
        accessor: 'deleteDoctor',
    },
    {
        Header: 'Update',
        accessor: 'updateDoctor',
    }
];

const filters = [
    {
        accessor: 'Name',
    }
];

class DoctorTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData

        };

    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />

        )
    }
}

export default DoctorTable;
