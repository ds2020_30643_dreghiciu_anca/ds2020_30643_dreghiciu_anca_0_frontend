import React from 'react';
import validate from "./validators/doctor-validator";
import Button from "react-bootstrap/Button";
import * as API_DOCTOR from "../api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';


class DoctorFormUpdate extends React.Component{
    constructor(props) {
        super(props);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.reloadUpdate = this.props.reloadUpdate;
        this.id = this.props.doctorId;
        this.fetchDoctors = this.props.fetchDoctors;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'Enter an new name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                birthdate: {
                     value: '',
                     placeholder: 'Enter a new birthdate...',
                     valid: false,
                     touched: false,
                     validationRules: {
                     isRequired: true
                     }
                },
                specialization: {
                    value: '',
                    placeholder: 'New specialization',
                    valid: false,
                    touched: false,

                },

                address: {
                    value: '',
                    placeholder: 'new Address',
                    valid: false,
                    touched: false,
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.updateDoctor = this.updateDoctor.bind(this);
        //this.reloadUpdate = this.reloadUpdate.bind(this);

    }

    toggleFormUpdate(idDoctor) {
        this.setState({collapseForm: !this.state.collapseForm});
    }




    handleChange = event => {

        const name = event.target.name;
                const value = event.target.value;

                const updatedControls = {
                    ...this.state.formControls
                };

                const updatedFormElement = {
                    ...updatedControls[name]
                };

                updatedFormElement.value = value;
                updatedFormElement.touched = true;
                updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

                console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

                updatedControls[name] = updatedFormElement;

                let formIsValid = true;
                for (let updatedFormElementName in updatedControls) {
                    formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
                }

                this.setState({
                    formControls: updatedControls,
                    formIsValid: formIsValid
                });


    };



    updateDoctor = (doctor) => {
        console.log("HAI MA ID: " + this.id);
        return API_DOCTOR.updateDoctor(doctor, this.id, (result, status, err) =>{
            if(result !== null && (status === 200 || status === 201)){
                console.log("Vrea dar nu se poate?");
                this.reloadHandler();
            }
            else{
            console.log("O intrat in eroare!!!!");
                this.setState({
                    errorStatus: status,
                    error: err
                })
            }

        });
    }

    handleUpdate() {
         console.log("New doctor data:");
         console.log("Name: " + this.state.formControls.name.value);
         console.log("Birthdate: " + this.state.formControls.birthdate.value);
         console.log("Specialization: " + this.state.formControls.specialization.value);
         console.log("Address: " + this.state.formControls.address.value);

        let doctor = {
            name: this.state.formControls.name.value,
            specialization: this.state.formControls.specialization.value,
            birthdate: this.state.formControls.birthdate.value,
            address: this.state.formControls.address.value,
        };
        console.log(doctor+"ID din handle" + this.id);
        this.updateDoctor(doctor);
    }


    render() {
            return (
                <div>
                    <FormGroup id='name'>
                        <Label for='nameField'> Name: </Label>
                        <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.name.value}
                               touched={this.state.formControls.name.touched? 1 : 0}
                               valid={this.state.formControls.name.valid}
                               required
                        />
                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                    </FormGroup>

                    <FormGroup id='birthdate'>
                        <Label for='birthdateField'> Birthdate: </Label>
                        <Input name='birthdate' id='birthdateField' placeholder={this.state.formControls.birthdate.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.birthdate.value}
                               touched={this.state.formControls.birthdate.touched? 1 : 0}
                               valid={this.state.formControls.birthdate.valid}
                               required
                        />
                        {this.state.formControls.birthdate.touched && !this.state.formControls.birthdate.valid &&
                        <div className={"error-message row"}> * Birthdate must have at least 8 characters </div>}
                    </FormGroup>

                    <FormGroup id='specialization'>
                        <Label for='specializationField'> Specialization: </Label>
                        <Input name='specialization'
                               placeholder={this.state.formControls.specialization.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.specialization.value}
                               touched={this.state.formControls.specialization.touched? 1 : 0}
                               valid={this.state.formControls.specialization.valid}
                               required
                        />

                    </FormGroup>

                    <FormGroup id='address'>
                        <Label for='addressField'> Address: </Label>
                        <Input name='address'
                               placeholder={this.state.formControls.address.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.address.value}
                               touched={this.state.formControls.address.touched? 1 : 0}
                               valid={this.state.formControls.address.valid}
                               required
                        />
                    </FormGroup>

                        <Row>
                            <Col sm={{size: '4', offset: 8}}>
                                <Button type={"update"} onClick={this.handleUpdate}>  Update </Button>
                            </Col>
                        </Row>


                    {
                        this.state.errorStatus > 0 &&
                        <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                    }
                </div>
            ) ;
        }

}
export default DoctorFormUpdate;