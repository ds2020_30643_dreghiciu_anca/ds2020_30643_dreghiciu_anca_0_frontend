import React from "react";
import Table from "../../commons/tables/table";




const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Birthdate',
        accessor: 'birthDate',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Patients',
        accessor: 'patients',
    },
    {
        Header: 'Delete',
        accessor: 'deleteCaregiver',
    },
    {
        Header: 'Update',
        accessor: 'updateCaregiver',
    }

];

const filters = [
    {
        accessor: 'Name',
    }
];

class CaregiverTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData

        };

    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />

        )
    }
}

export default CaregiverTable;
