import React from 'react';
import validate from "./validators/caregiver-validator";
import Button from "react-bootstrap/Button";
import * as API_CAREGIVER from "../api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';


class CaregiverFormUpdate extends React.Component{
    constructor(props) {
        super(props);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.reloadUpdate = this.props.reloadUpdate;
        this.id = this.props.caregiverId;
        this.fetchCaregivers = this.props.fetchCaregivers;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'Enter an new name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                birthDate: {
                     value: '',
                     placeholder: 'Enter a new birthdate...',
                     valid: false,
                     touched: false,
                     validationRules: {
                     isRequired: true
                     }
                },
                gender: {
                    value: '',
                    placeholder: 'New gender',
                    valid: false,
                    touched: false,

                },
                address: {
                    value: '',
                    placeholder: 'new Address',
                    valid: false,
                    touched: false,
                },




            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.updateCaregiver = this.updateCaregiver.bind(this);
        //this.reloadUpdate = this.reloadUpdate.bind(this);

    }

    toggleFormUpdate(idCaregiver) {
        this.setState({collapseForm: !this.state.collapseForm});
    }




    handleChange = event => {

        const name = event.target.name;
                const value = event.target.value;

                const updatedControls = {
                    ...this.state.formControls
                };

                const updatedFormElement = {
                    ...updatedControls[name]
                };

                updatedFormElement.value = value;
                updatedFormElement.touched = true;
                updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

                console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

                updatedControls[name] = updatedFormElement;

                let formIsValid = true;
                for (let updatedFormElementName in updatedControls) {
                    formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
                }

                this.setState({
                    formControls: updatedControls,
                    formIsValid: formIsValid
                });


    };



    updateCaregiver = (caregiver) => {
        console.log("HAI MA ID: " + this.id);
        return API_CAREGIVER.updateCaregiver(caregiver, this.id, (result, status, err) =>{
            if(result !== null && (status === 200 || status === 201)){
                console.log("Vrea dar nu se poate?");
                this.reloadHandler();
            }
            else{
            console.log("O intrat in eroare!!!!");
                this.setState({
                    errorStatus: status,
                    error: err
                })
            }

        });
    }

    handleUpdate() {
         console.log("New caregiver data:");
         console.log("Name: " + this.state.formControls.name.value);
         console.log("Birthdate: " + this.state.formControls.birthDate.value);
         console.log("Gender: " + this.state.formControls.gender.value);
         console.log("Address: " + this.state.formControls.address.value);


        let caregiver = {

            name: this.state.formControls.name.value,
            birthDate: this.state.formControls.birthDate.value,
            address: this.state.formControls.address.value,
            gender: this.state.formControls.gender.value,

        };

        console.log(caregiver + "ID din handle" + this.id);

        this.updateCaregiver(caregiver);
    }


    render() {
            return (
                <div>
                    <FormGroup id='name'>
                        <Label for='nameField'> Name: </Label>
                        <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.name.value}
                               touched={this.state.formControls.name.touched? 1 : 0}
                               valid={this.state.formControls.name.valid}
                               required
                        />
                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                        <div className={"error-message row"}> * Username must have at least 3 characters </div>}
                    </FormGroup>

                    <FormGroup id='birthDate'>
                        <Label for='birthDateField'> Birthdate: </Label>
                        <Input name='birthDate' id='birthdateField' placeholder={this.state.formControls.birthDate.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.birthDate.value}
                               touched={this.state.formControls.birthDate.touched? 1 : 0}
                               valid={this.state.formControls.birthDate.valid}
                               required
                        />
                    </FormGroup>

                    <FormGroup id='gender'>
                        <Label for='genderField'> Gender: </Label>
                        <Input name='gender'
                               placeholder={this.state.formControls.gender.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.gender.value}
                               touched={this.state.formControls.gender.touched? 1 : 0}
                               valid={this.state.formControls.gender.valid}
                               required
                        />
                    </FormGroup>

                    <FormGroup id='address'>
                        <Label for='addressField'> Address: </Label>
                        <Input name='address'
                               placeholder={this.state.formControls.address.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.address.value}
                               touched={this.state.formControls.address.touched? 1 : 0}
                               valid={this.state.formControls.address.valid}
                               required
                        />
                    </FormGroup>



                        <Row>
                            <Col sm={{size: '4', offset: 8}}>
                                <Button type={"update"} onClick={this.handleUpdate}>  Update </Button>
                            </Col>
                        </Row>


                    {
                        this.state.errorStatus > 0 &&
                        <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                    }
                </div>
            ) ;
        }

}
export default CaregiverFormUpdate;