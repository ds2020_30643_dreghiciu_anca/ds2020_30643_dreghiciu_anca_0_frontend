import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import CaregiverForm from "./components/caregiver-form-add";
import CaregiverFormUpdate from "./components/caregiver-form-update";

import * as API_CAREGIVER from "./api/caregiver-api"
import CaregiverTable from "./components/caregiver-table";



class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);

        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selectedUpdate: false,
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
        this.handleDeleteButton = this.handleDeleteButton.bind(this);
        this.addButtons = this.addButtons.bind(this);
        this.updateCaregiver = this.updateCaregiver.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);

    }



    componentDidMount() {
        this.fetchCaregivers();
    }
    reloadDelete() {
        this.setState({
           isLoaded: false
           });
        this.fetchCaregivers();
    }

    handleDeleteButton = (idCaregiver) => {

        return API_CAREGIVER.deleteCaregiver(idCaregiver, (result, status, err) =>{
            if(result !== null && status === 200) {
                this.reloadDelete();
            }
            else{
                this.setState({
                    errorStatus: status,
                    error: err
                })
            }

        });
    }

    updateCaregiver = (caregiver) =>
    {
        console.log("Id caregiver for update: "+caregiver.id);
        this.setState({
            idCaregiver: caregiver.id
        })

        console.log("stare id: "+ this.idCaregiver);
        this.toggleFormUpdate(caregiver);
    }

    addButtons = (data) => {
        const newData = data.map( caregiver => {
            return{
                ...caregiver,
                deleteCaregiver:
                    <Button color = "danger" onClick={this.handleDeleteButton.bind(this, caregiver.id)}>
                        Delete
                    </Button>,
                updateCaregiver:
                    <Button color="info" onClick={this.updateCaregiver.bind(this, caregiver)}>
                        Update
                    </Button>

            }

        });
        return newData;
    }


    fetchCaregivers() {
        return API_CAREGIVER.getCaregivers((result, status, err) => {
        if(result !== null && status === 200) {
           let newTableData = this.addButtons(result);
           this.setState({
             tableData: newTableData,
             isLoaded: true
           })
        }
        else
            {
            this.setState({
                errorStatus: status,
                error: err
            });
            }

        });

    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleFormUpdate(idCaregiver)
    {
        console.log("id caregiver in toggle dar in container: "+ idCaregiver);
        this.setState({selectedUpdate: !this.state.selectedUpdate});

    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCaregivers();
    }



    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Caregivers Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Caregiver </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <CaregiverTable tableData = {this.state.tableData}/>
                            }
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>


                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}> Update Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverFormUpdate reloadHandler={this.reload} caregiverId={this.state.idCaregiver}/>
                    </ModalBody>
                </Modal>
            </div>

        )

    }
}


export default CaregiverContainer;
