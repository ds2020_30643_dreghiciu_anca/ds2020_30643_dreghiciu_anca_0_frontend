import React from 'react';
import validate from "./validators/medication-validators";
import Button from "react-bootstrap/Button";
import * as API_MEDICATION from "../api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';

class MedicationFormUpdate extends React.Component{
    constructor(props) {
        super(props);
        this.toggleFormUpdateMedication = this.toggleFormUpdateMedication.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        //this.reloadUpdate = this.props.reloadUpdate;
        this.id = this.props.medicationId;
        this.fetchMedications = this.props.fetchMedications;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'Enter an new name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                sideEffects: {
                     value: '',
                     placeholder: 'Enter a new Side Effects...',
                     valid: false,
                     touched: false,
                     validationRules: {
                     isRequired: true
                     }
                },
                dosage: {
                    value: '',
                    placeholder: 'New dosage',
                    valid: false,
                    touched: false
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleUpdateMedication = this.handleUpdateMedication.bind(this);
        this.updateMedication = this.updateMedication.bind(this);
        //this.reloadUpdate = this.reloadUpdate.bind(this);

    }

    toggleFormUpdateMedication(idMedication) {
        this.setState({collapseForm: !this.state.collapseForm});
    }




    handleChange = event => {

        const name = event.target.name;
                const value = event.target.value;

                const updatedControls = {
                    ...this.state.formControls
                };

                const updatedFormElement = {
                    ...updatedControls[name]
                };

                updatedFormElement.value = value;
                updatedFormElement.touched = true;
                updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

                console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

                updatedControls[name] = updatedFormElement;

                let formIsValid = true;
                for (let updatedFormElementName in updatedControls) {
                    formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
                }

                this.setState({
                    formControls: updatedControls,
                    formIsValid: formIsValid
                });


    };



    updateMedication = (medication) => {
        console.log("HAI MA ID: " + this.id);
        return API_MEDICATION.updateMedication(medication, this.id, (result, status, err) =>{
            if(result !== null && (status === 200 || status === 201)){
                console.log("Vrea dar nu se poate?");
                this.reloadHandler();
            }
            else{
            console.log("O intrat in eroare!!!!");
                this.setState({
                    errorStatus: status,
                    error: err
                })
            }

        });
    }

    handleUpdateMedication() {
         console.log("New Medication data:");
         console.log("Name: " + this.state.formControls.name.value);
         console.log("Side Effects: " + this.state.formControls.sideEffects.value);
         console.log("Dosage: " + this.state.formControls.dosage.value);

        let medication = {

            name: this.state.formControls.name.value,
            sideEffects: this.state.formControls.sideEffects.value,
            dosage: this.state.formControls.dosage.value,

        };

        console.log(medication + "ID din handle" + this.id);

        this.updateMedication(medication);
    }


    render() {
            return (
                <div>
                    <FormGroup id='name'>
                        <Label for='nameField'> Name: </Label>
                        <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.name.value}
                               touched={this.state.formControls.name.touched? 1 : 0}
                               valid={this.state.formControls.name.valid}
                               required
                        />
                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                    </FormGroup>

                    <FormGroup id='sideEffects'>
                        <Label for='sideEffectsField'> Side Effects: </Label>
                        <Input name='sideEffects' id='sideEffectsField' placeholder={this.state.formControls.sideEffects.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.sideEffects.value}
                               touched={this.state.formControls.sideEffects.touched? 1 : 0}
                               valid={this.state.formControls.sideEffects.valid}
                               required
                        />
                        {this.state.formControls.sideEffects.touched && !this.state.formControls.sideEffects.valid &&
                        <div className={"error-message row"}> * Side effects must have at least 8 characters </div>}
                    </FormGroup>

                    <FormGroup id='dosage'>
                        <Label for='dosageField'> Dosage: </Label>
                        <Input name='dosage'
                               placeholder={this.state.formControls.dosage.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.dosage.value}
                               touched={this.state.formControls.dosage.touched? 1 : 0}
                               valid={this.state.formControls.dosage.valid}
                               required
                        />
                        {this.state.formControls.dosage.touched && !this.state.formControls.dosage.valid &&
                        <div className={"error-message"}> * Dosage must have a valid format</div>}
                    </FormGroup>




                        <Row>
                            <Col sm={{size: '4', offset: 8}}>
                                <Button type={"update"} onClick={this.handleUpdateMedication}>  Update </Button>
                            </Col>
                        </Row>


                    {
                        this.state.errorStatus > 0 &&
                        <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                    }
                </div>
            ) ;
        }


}
export default MedicationFormUpdate;