import React from "react";
import Table from "../../commons/tables/table";

const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Side Effects',
        accessor: 'sideEffects',
    },
    {
        Header: 'Dosage',
        accessor: 'dosage',
    },
    {
        Header: 'Delete',
        accessor: 'deleteMedication',
    },
    {
        Header: 'Update',
        accessor: 'updateMedication',
    }

];

const filters = [
    {
        accessor: 'name',
    }
];

class MedicationTable extends React.Component{

constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData

        };

    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />

        )
    }
}
export default MedicationTable;