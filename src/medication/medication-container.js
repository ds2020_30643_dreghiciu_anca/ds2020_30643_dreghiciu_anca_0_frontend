import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import MedicationTable from "./components/medication-table";
import * as API_MEDICATION from "./api/medication-api"

import MedicationForm from "./components/medication-form-add";
import MedicationFormUpdate from "./components/medication-form-update";

class MedicationContainer extends React.Component{
    constructor(props) {
        super(props);

        this.toggleFormMedication = this.toggleFormMedication.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selectedUpdate: false,
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
        this.handleDeleteButton = this.handleDeleteButton.bind(this);
        this.addButtons = this.addButtons.bind(this);
        this.updateMedication = this.updateMedication.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.toggleFormUpdateMedication = this.toggleFormUpdateMedication.bind(this);

    }

    componentDidMount() {
            this.fetchMedications();
        }
        reloadDelete() {
            this.setState({
               isLoaded: false
               });
            this.fetchMedications();
        }

        handleDeleteButton = (idMedication) => {
            //console.log( "Id person for delete: "+ idPerson);
            return API_MEDICATION.deleteMedication(idMedication, (result, status, err) =>{
                if(result !== null && status === 200) {
                    this.reloadDelete();
                }
                else{
                    this.setState({
                        errorStatus: status,
                        error: err
                    })
                }

            });
        }

        updateMedication = (medication) =>
        {
            console.log("Id medication for update: "+medication.id);
            this.setState({
                idMedication: medication.id
            })

            console.log("stare id: "+ this.idMedication);
            this.toggleFormMedicationUpdate(medication);
        }

        addButtons = (data) => {
            const newData = data.map( medication => {
                return{
                    ...medication,
                    deleteMedication:
                        <Button color = "danger" onClick={this.handleDeleteButton.bind(this, medication.id)}>
                            Delete
                        </Button>,
                    updateMedication:
                        <Button color="info" onClick={this.updateMedication.bind(this, medication)}>
                            Update
                        </Button>

                }

            });
            return newData;
        }


        fetchMedications() {
            return API_MEDICATION.getMedications((result, status, err) => {
            if(result !== null && status === 200) {
               let newTableData = this.addButtons(result);
               this.setState({
                 tableData: newTableData,
                 isLoaded: true
               })
            }
            else
                {
                this.setState({
                    errorStatus: status,
                    error: err
                });
                }

            });

        }

        toggleFormMedication() {
            this.setState({selected: !this.state.selected});
        }

        toggleFormUpdateMedication(idMedication)
        {
            console.log("id medication in toggle dar in container: "+ idMedication);
            this.setState({selectedUpdate: !this.state.selectedUpdate});
        }


        reload() {
            this.setState({
                isLoaded: false
            });
            this.toggleFormMedication();
            this.fetchMedications();
        }



        render() {
            return (
                <div>
                    <CardHeader>
                        <strong> Medication Management </strong>
                    </CardHeader>
                    <Card>
                        <br/>
                        <Row>
                            <Col sm={{size: '8', offset: 1}}>
                                <Button color="primary" onClick={this.toggleFormMedication}>Add Medication </Button>
                            </Col>
                        </Row>
                        <br/>
                        <Row>
                            <Col sm={{size: '8', offset: 1}}>
                                {this.state.isLoaded && <MedicationTable tableData = {this.state.tableData}/>
                                }
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                                errorStatus={this.state.errorStatus}
                                                                error={this.state.error}
                                                            />   }
                            </Col>
                        </Row>
                    </Card>

                    <Modal isOpen={this.state.selected} toggle={this.toggleFormMedication}
                        className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormMedication}> Add Medication: </ModalHeader>
                            <ModalBody>
                                <MedicationForm reloadHandler={this.reload}/>
                            </ModalBody>
                    </Modal>
                    <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormMedicationUpdate}
                        className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormMedicationUpdate}> Update Medication: </ModalHeader>
                            <ModalBody>
                                <MedicationFormUpdate reloadHandler={this.reload} medicationId={this.state.idMedication}/>
                            </ModalBody>
                    </Modal>
                </div>

            )

        }

} export default MedicationContainer;