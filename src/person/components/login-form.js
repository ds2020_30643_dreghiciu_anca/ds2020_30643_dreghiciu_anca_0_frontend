import React from 'react';
import validate from "./validators/person-validators";
import TextInput from "./TextInput";

import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {withRouter} from 'react-router-dom';
import * as API_LOGIN from "../api/login-api";
import Input from "reactstrap/es/Input";

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            role: '',
            errorStatus: 0,
            error: null,

            formIsValid: false,

            username: {
                value: '',
                placeholder: 'username...',
                valid: false,
                touched: false,
                validationRules: {
                    minLength: 5,
                    isRequired: true
                }
            },

            password: {
                value: '',
                placeholder: 'password',
                valid: false,
                touched: false,
                validationRules: {
                    emailValidator: true
                }
            },


            userId: 0

        };

        this.handleChange = this.handleChange.bind(this);

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const rules = this.state.username.validationRules;
        if (name === "username") {
            this.setState(
                {
                    username:
                        {
                            valid: validate(value, rules),
                            value: value


                        }
                }
            )
        } else {
            this.setState({

                password: {
                    valid: validate(value, rules),
                    value: value

                }
            });
        }
    };

    registerLogin(user){
        return API_LOGIN.login(user, (result, status, error) => {
          console.log(result);

          if (result !== null && (status === 200 || status === 201)) {
             this.setState({
             role: result.role });
             localStorage.setItem("role",result.role);
             localStorage.setItem("userId",result.userId);
             user.username = result.username;
             user.role = result.role;
             if (user.role==="patient")
                this.props.history.push("/patient" );
             else
               if (user.role==="doctor")
                this.props.history.push("/doctor" );
               if (user.role==="caregiver")
                  this.props.history.push("/caregiver" );
               else
                 {


                 }
               } else {
                  this.setState(({
                      errorStatus: status,
                      error: error
                  }));
                  }
               });
    }



      handleSubmit(e) {

              e.preventDefault();
               console.log("User log in:");
               console.log("Username: " + this.state.username.value);

               let user = {
                   username: this.state.username.value,
                   password: this.state.password.value

               };
               this.registerLogin(user);
      }







    render() {
        return (

            <form className={'LoginForm'}>

                <h1 className={'LoginTitle'}>Log In</h1>

                <p> Username: </p>

                <TextInput name="username"
                           placeholder={this.state.username.placeholder}
                           value={this.state.username.value}
                           onChange={this.handleChange}
                           touched={this.state.username.touched}
                           valid={this.state.username.valid}
                />
                {this.state.username.touched && !this.state.username.valid &&
                <div className={"error-message row"}> * username must have at least 5 characters </div>}

                <p> Password: </p>
                <TextInput name="password"
                           type={"password"}
                           placeholder={this.state.password.placeholder}
                           value={this.state.password.value}
                           onChange={this.handleChange}
                           touched={this.state.password.touched}
                           valid={this.state.password.valid}
                />
                {this.state.password.touched && !this.state.password.valid &&
                <div className={"error-message"}> * Password must have a valid format</div>}



                <Button variant="success"
                        type={"submit"}
                        onClick={(e) => this.handleSubmit(e)}
                        disabled={!this.state.username.valid || !this.state.password.valid}>
                   Log in
                </Button>


                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>

        );
    }
}

export default withRouter(LoginForm);
