import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { FormGroup, Input, Label} from 'reactstrap';
import Button from "react-bootstrap/Button";
import {Card, Col, Row} from 'reactstrap';
import LoginForm from "./login-form";
import {withRouter} from "react-router-dom";



class Login extends React.Component {
constructor(props){
        super(props);

        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };
    }

    render() {
    return(
        <div>
            <Row>
                <Col>
                    <Card body>
                        <div>
                            <LoginForm/>

                        </div>
                    </Card>
                </Col>
            </Row>

            this.state.errorStatus > 0 &&
            <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>

        </div>
    );
    }

}
export default Login;

