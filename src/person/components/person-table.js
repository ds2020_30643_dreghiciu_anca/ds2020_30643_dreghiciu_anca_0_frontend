import React from "react";
import Table from "../../commons/tables/table";




const columns = [
    {
        Header: 'Username',
        accessor: 'username',
    },
    {
        Header: 'Password',
        accessor: 'password',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Header: 'Email',
        accessor: 'email',
    },
    {
        Header: 'Age',
        accessor: 'age',
    },
    {
        Header: 'Role',
        accessor: 'role',
    },
    {
        Header: 'Delete',
        accessor: 'deletePerson',
    },
    {
        Header: 'Update',
        accessor: 'updatePerson',
    }
];

const filters = [
    {
        accessor: 'Name',
    }
];

class PersonTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData

        };

    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />

        )
    }
}

export default PersonTable;
