import React from 'react';
import validate from "./validators/person-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/person-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';


class PersonFormUpdate extends React.Component{
    constructor(props) {
        super(props);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.reloadUpdate = this.props.reloadUpdate;
        this.id = this.props.personId;
        this.fetchPersons = this.props.fetchPersons;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                username: {
                    value: '',
                    placeholder: 'Enter an new username...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                password: {
                     value: '',
                     placeholder: 'Enter a new password...',
                     valid: false,
                     touched: false,
                     validationRules: {
                     isRequired: true
                     }
                },
                email: {
                    value: '',
                    placeholder: 'New email',
                    valid: false,
                    touched: false,
                    validationRules: {
                        emailValidator: true
                    }
                },
                age: {
                    value: '',
                    placeholder: 'new Age',
                    valid: false,
                    touched: false,
                },
                address: {
                    value: '',
                    placeholder: 'new Address',
                    valid: false,
                    touched: false,
                },
                role: {
                    value: '',
                    placeholder: 'new Role (Patient, Doctor or Caregiver)',
                    valid: false,
                    touched: false,
                },



            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.updatePerson = this.updatePerson.bind(this);
        //this.reloadUpdate = this.reloadUpdate.bind(this);

    }

    toggleFormUpdate(idPerson) {
        this.setState({collapseForm: !this.state.collapseForm});
    }




    handleChange = event => {

        const name = event.target.name;
                const value = event.target.value;

                const updatedControls = {
                    ...this.state.formControls
                };

                const updatedFormElement = {
                    ...updatedControls[name]
                };

                updatedFormElement.value = value;
                updatedFormElement.touched = true;
                updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

                console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

                updatedControls[name] = updatedFormElement;

                let formIsValid = true;
                for (let updatedFormElementName in updatedControls) {
                    formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
                }

                this.setState({
                    formControls: updatedControls,
                    formIsValid: formIsValid
                });


    };



    updatePerson = (person) => {
        console.log("HAI MA ID: " + this.id);
        return API_USERS.updatePerson(person, this.id, (result, status, err) =>{
            if(result !== null && (status === 200 || status === 201)){
                console.log("Vrea dar nu se poate?");
                this.reloadHandler();
            }
            else{
            console.log("O intrat in eroare!!!!");
                this.setState({
                    errorStatus: status,
                    error: err
                })
            }

        });
    }

    handleUpdate() {
         console.log("New person data:");
         console.log("Name: " + this.state.formControls.username.value);
         console.log("Email: " + this.state.formControls.email.value);
         console.log("password: " + this.state.formControls.password.value);
         console.log("Address: " + this.state.formControls.address.value);
         console.log("Age: " + this.state.formControls.age.value);
         console.log("Role: " + this.state.formControls.role.value);

        let person = {

            username: this.state.formControls.username.value,
            address: this.state.formControls.address.value,
            password: this.state.formControls.password.value,
            age: this.state.formControls.age.value,
            email: this.state.formControls.email.value,
            role: this.state.formControls.role.value,
        };

        console.log(person + "ID din handle" + this.id);

        this.updatePerson(person);
    }


    render() {
            return (
                <div>
                    <FormGroup id='username'>
                        <Label for='userNameField'> Username: </Label>
                        <Input name='username' id='userNameField' placeholder={this.state.formControls.username.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.username.value}
                               touched={this.state.formControls.username.touched? 1 : 0}
                               valid={this.state.formControls.username.valid}
                               required
                        />
                        {this.state.formControls.username.touched && !this.state.formControls.username.valid &&
                        <div className={"error-message row"}> * Username must have at least 3 characters </div>}
                    </FormGroup>

                    <FormGroup id='password'>
                        <Label for='passwordField'> Password: </Label>
                        <Input name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.password.value}
                               touched={this.state.formControls.password.touched? 1 : 0}
                               valid={this.state.formControls.password.valid}
                               required
                        />
                        {this.state.formControls.password.touched && !this.state.formControls.username.valid &&
                        <div className={"error-message row"}> * Password must have at least 8 characters </div>}
                    </FormGroup>

                    <FormGroup id='email'>
                        <Label for='emailField'> Email: </Label>
                        <Input name='email'
                               placeholder={this.state.formControls.email.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.email.value}
                               touched={this.state.formControls.email.touched? 1 : 0}
                               valid={this.state.formControls.email.valid}
                               required
                        />
                        {this.state.formControls.email.touched && !this.state.formControls.email.valid &&
                        <div className={"error-message"}> * Email must have a valid format</div>}
                    </FormGroup>

                    <FormGroup id='address'>
                        <Label for='addressField'> Address: </Label>
                        <Input name='address'
                               placeholder={this.state.formControls.address.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.address.value}
                               touched={this.state.formControls.address.touched? 1 : 0}
                               valid={this.state.formControls.address.valid}
                               required
                        />
                    </FormGroup>

                    <FormGroup id='age'>
                        <Label for='ageField'> Age: </Label>
                        <Input name='age' id='ageField'
                               placeholder={this.state.formControls.age.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.age.value}
                               touched={this.state.formControls.age.touched? 1 : 0}
                               valid={this.state.formControls.age.valid}
                               required
                        />
                    </FormGroup>
                    <FormGroup id='role'>
                        <Label for='roleField'> Role: </Label>
                        <Input name='role' id='roleField' placeholder={this.state.formControls.role.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.role.value}
                               touched={this.state.formControls.role.touched? 1 : 0}
                               valid={this.state.formControls.role.valid}
                               required
                        />
                    </FormGroup>


                        <Row>
                            <Col sm={{size: '4', offset: 8}}>
                                <Button type={"update"} onClick={this.handleUpdate}>  Update </Button>
                            </Col>
                        </Row>


                    {
                        this.state.errorStatus > 0 &&
                        <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                    }
                </div>
            ) ;
        }

}
export default PersonFormUpdate;