import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PersonForm from "./components/person-form";
import PersonFormUpdate from "./components/person-form-update";

import * as API_USERS from "./api/person-api"
import PersonTable from "./components/person-table";



class PersonContainer extends React.Component {

    constructor(props) {
        super(props);

        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selectedUpdate: false,
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
        this.handleDeleteButton = this.handleDeleteButton.bind(this);
        this.addButtons = this.addButtons.bind(this);
        this.updatePerson = this.updatePerson.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);

    }



    componentDidMount() {
        this.fetchPersons();
    }
    reloadDelete() {
        this.setState({
           isLoaded: false
           });
        this.fetchPersons();
    }

    handleDeleteButton = (idPerson) => {
        //console.log( "Id person for delete: "+ idPerson);
        return API_USERS.deletePerson(idPerson, (result, status, err) =>{
            if(result !== null && status === 200) {
                this.reloadDelete();
            }
            else{
                this.setState({
                    errorStatus: status,
                    error: err
                })
            }

        });
    }

    updatePerson = (person) =>
    {
        console.log("Id person for update: "+person.id);
        this.setState({
            idPerson: person.id
        })

        console.log("stare id: "+ this.idPerson);
        this.toggleFormUpdate(person);
    }

    addButtons = (data) => {
        const newData = data.map( person => {
            return{
                ...person,
                deletePerson:
                    <Button color = "danger" onClick={this.handleDeleteButton.bind(this, person.id)}>
                        Delete
                    </Button>,
                updatePerson:
                    <Button color="info" onClick={this.updatePerson.bind(this, person)}>
                        Update
                    </Button>

            }

        });
        return newData;
    }


    fetchPersons() {
        return API_USERS.getPersons((result, status, err) => {
        if(result !== null && status === 200) {
           let newTableData = this.addButtons(result);
           this.setState({
             tableData: newTableData,
             isLoaded: true
           })
        }
        else
            {
            this.setState({
                errorStatus: status,
                error: err
            });
            }

        });

    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleFormUpdate(idPerson)
    {
        console.log("id person in toggle dar in container: "+ idPerson);
        this.setState({selectedUpdate: !this.state.selectedUpdate});

    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPersons();
    }



    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Person Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Person </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PersonTable tableData = {this.state.tableData}/>
                            }
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>


                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Person: </ModalHeader>
                    <ModalBody>
                        <PersonForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}> Update Person: </ModalHeader>
                    <ModalBody>
                        <PersonFormUpdate reloadHandler={this.reload} personId={this.state.idPerson}/>
                    </ModalBody>
                </Modal>
            </div>

        )

    }
}


export default PersonContainer;
