import React from "react";
import Table from "../../commons/tables/table";

const columns = [
    {
        Header: 'Medication Plan Id',
        accessor: 'medicationPlanId',
    },
    {
        Header: 'Intake Interval Plan',
        accessor: 'intakeInterval',
    },
    {
        Header: 'Start Date',
        accessor: 'startDate',
    },
    {
        Header: 'End Date',
        accessor: 'endDate',
    },
    {
        Header: 'Patient Id',
        accessor: 'patientId',
    },
    {
        Header: 'Plan Id',
        accessor: 'medicationToPlanDTOS[0].id',
    },
    {
        Header: 'Med Id',
        accessor: 'medicationToPlanDTOS[0].medicationDTO.id',

    },
    {
        Header: 'Med Name',
        accessor: 'medicationToPlanDTOS[0].medicationDTO.name',
    },
    {
        Header: 'Side Effects',
        accessor: 'medicationToPlanDTOS[0].medicationDTO.sideEffects',
    },
    {
        Header: 'Med Dosage',
        accessor: 'medicationToPlanDTOS[0].medicationDTO.dosage',
    },
    {
        Header: 'Med Intake Interval',
        accessor: 'medicationToPlanDTOS[0].intakeInterval',
    },
    {
        Header: 'Plan Dosage',
        accessor: 'medicationToPlanDTOS[0].dosage',
    }

];

const filters = [
    {
        accessor: 'Name',
    }
];

class MedicationPlanTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData

        };

    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />

        )
    }
}

export default MedicationPlanTable;
