import React from 'react';
import validate from "./validators/patient-validator";
import Button from "react-bootstrap/Button";
import * as API_PATIENT from "../api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';


class PatientFormUpdate extends React.Component{
    constructor(props) {
        super(props);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.reloadUpdate = this.props.reloadUpdate;
        this.id = this.props.patientId;
        this.fetchPersons = this.props.fetchPersons;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: 'Enter an new name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                birthDate: {
                     value: '',
                     placeholder: 'Enter a new birthdate...',
                     valid: false,
                     touched: false,
                     validationRules: {
                     isRequired: true
                     }
                },
                gender: {
                    value: '',
                    placeholder: 'New gender',
                    valid: false,
                    touched: false,

                },

                address: {
                    value: '',
                    placeholder: 'new Address',
                    valid: false,
                    touched: false,
                },
                medicalRecord: {
                    value: '',
                    placeholder: 'new Medical Record',
                    valid: false,
                    touched: false,
                },
                caregiverId: {
                    value: '',
                    placeholder: 'new Caregiver Id..',
                    valid: false,
                    touched: false,
                },
                caregiverName: {
                    value: '',
                    placeholder: 'new Caregiver Name..',
                    valid: false,
                    touched: false,
                },
                doctorId: {
                    value: '',
                    placeholder: 'new Doctor Id..',
                    valid: false,
                    touched: false,
                },
                doctorName: {
                    value: '',
                    placeholder: 'new Doctor Name..',
                    valid: false,
                    touched: false,
                },



            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.updatePatient = this.updatePatient.bind(this);
        //this.reloadUpdate = this.reloadUpdate.bind(this);

    }

    toggleFormUpdate(idPatient) {
        this.setState({collapseForm: !this.state.collapseForm});
    }




    handleChange = event => {

        const name = event.target.name;
                const value = event.target.value;

                const updatedControls = {
                    ...this.state.formControls
                };

                const updatedFormElement = {
                    ...updatedControls[name]
                };

                updatedFormElement.value = value;
                updatedFormElement.touched = true;
                updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

                console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

                updatedControls[name] = updatedFormElement;

                let formIsValid = true;
                for (let updatedFormElementName in updatedControls) {
                    formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
                }

                this.setState({
                    formControls: updatedControls,
                    formIsValid: formIsValid
                });


    };



    updatePatient = (patient) => {
        console.log("HAI MA ID: " + this.id);
        return API_PATIENT.updatePatient(patient, this.id, (result, status, err) =>{
            if(result !== null && (status === 200 || status === 201)){
                console.log("Vrea dar nu se poate?");
                this.reloadHandler();
            }
            else{
            console.log("O intrat in eroare!!!!");
                this.setState({
                    errorStatus: status,
                    error: err
                })
            }

        });
    }

    handleUpdate() {
         console.log("New patient data:");
         console.log("Name: " + this.state.formControls.name.value);
         console.log("Birthdate: " + this.state.formControls.birthDate.value);
         console.log("Gender: " + this.state.formControls.gender.value);
         console.log("Address: " + this.state.formControls.address.value);
         console.log("Medical Record: " + this.state.formControls.medicalRecord.value);
         console.log("Caregiver Id: " + this.state.formControls.caregiverId.value);
         console.log("Caregiver Name: " + this.state.formControls.caregiverName.value);
         console.log("Doctor Id: " + this.state.formControls.doctorId.value);
         console.log("Doctor Name: " + this.state.formControls.doctorName.value);

        let patient = {

            name: this.state.formControls.name.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
            medicalRecord: this.state.formControls.medicalRecord.value,
            caregiverId: this.state.formControls.caregiverId.value,
            caregiverName: this.state.formControls.caregiverName.value,
            doctorId: this.state.formControls.doctorId.value,
            doctorName: this.state.formControls.doctorName.value,
        };

        console.log(patient + "ID din handle" + this.id);

        this.updatePatient(patient);
    }


    render() {
            return (
                <div>
                    <FormGroup id='name'>
                        <Label for='nameField'> Name: </Label>
                        <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.name.value}
                               touched={this.state.formControls.name.touched? 1 : 0}
                               valid={this.state.formControls.name.valid}
                               required
                        />
                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                    </FormGroup>

                    <FormGroup id='birthdate'>
                        <Label for='birthdateField'> Birthdate: </Label>
                        <Input name='birthdate' id='birthdateField' placeholder={this.state.formControls.birthDate.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.birthDate.value}
                               touched={this.state.formControls.birthDate.touched? 1 : 0}
                               valid={this.state.formControls.birthDate.valid}
                               required
                        />
                        {this.state.formControls.birthDate.touched && !this.state.formControls.birthDate.valid &&
                        <div className={"error-message row"}> * Birthdate must have at least 8 characters </div>}
                    </FormGroup>

                    <FormGroup id='gender'>
                        <Label for='genderField'> Gender: </Label>
                        <Input name='gender'
                               placeholder={this.state.formControls.gender.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.gender.value}
                               touched={this.state.formControls.gender.touched? 1 : 0}
                               valid={this.state.formControls.gender.valid}
                               required
                        />
                        {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
                        <div className={"error-message"}> * Gender must have a valid format</div>}
                    </FormGroup>

                    <FormGroup id='address'>
                        <Label for='addressField'> Address: </Label>
                        <Input name='address'
                               placeholder={this.state.formControls.address.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.address.value}
                               touched={this.state.formControls.address.touched? 1 : 0}
                               valid={this.state.formControls.address.valid}
                               required
                        />
                    </FormGroup>

                    <FormGroup id='medicalRecord'>
                        <Label for='medicalRecordField'> Medical Record: </Label>
                        <Input name='medicalRecord' id='medicalRecordField'
                               placeholder={this.state.formControls.medicalRecord.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.medicalRecord.value}
                               touched={this.state.formControls.medicalRecord.touched? 1 : 0}
                               valid={this.state.formControls.medicalRecord.valid}
                               required
                        />
                    </FormGroup>
                    <FormGroup id='caregiverId'>
                        <Label for='caregiverIdField'> Caregiver Id: </Label>
                        <Input name='caregiverId' id='caregiverIdField' placeholder={this.state.formControls.caregiverId.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.caregiverId.value}
                               touched={this.state.formControls.caregiverId.touched? 1 : 0}
                               valid={this.state.formControls.caregiverId.valid}
                               required
                        />
                    </FormGroup>
                    <FormGroup id='caregiverName'>
                        <Label for='caregiverNameField'> Caregiver Name: </Label>
                        <Input name='caregiverName' id='caregiverNameField' placeholder={this.state.formControls.caregiverName.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.caregiverName.value}
                               touched={this.state.formControls.caregiverName.touched? 1 : 0}
                               valid={this.state.formControls.caregiverName.valid}
                               required
                        />
                    </FormGroup>
                    <FormGroup id='doctorId'>
                        <Label for='doctorIddField'> Doctor Id: </Label>
                        <Input name='doctorId' id='doctorIdField' placeholder={this.state.formControls.doctorId.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.doctorId.value}
                               touched={this.state.formControls.doctorId.touched? 1 : 0}
                               valid={this.state.formControls.doctorId.valid}
                               required
                        />
                    </FormGroup>
                    <FormGroup id='doctorName'>
                        <Label for='doctorNameField'> Doctor Name: </Label>
                        <Input name='doctorName' id='doctorNameField' placeholder={this.state.formControls.doctorName.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.doctorName.value}
                               touched={this.state.formControls.doctorName.touched? 1 : 0}
                               valid={this.state.formControls.doctorName.valid}
                               required
                        />
                    </FormGroup>


                        <Row>
                            <Col sm={{size: '4', offset: 8}}>
                                <Button type={"update"} onClick={this.handleUpdate}>  Update </Button>
                            </Col>
                        </Row>


                    {
                        this.state.errorStatus > 0 &&
                        <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                    }
                </div>
            ) ;
        }

}
export default PatientFormUpdate;