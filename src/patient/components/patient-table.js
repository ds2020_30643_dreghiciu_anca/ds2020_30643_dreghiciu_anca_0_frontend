import React from "react";
import Table from "../../commons/tables/table";




const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Birthdate',
        accessor: 'birthDate',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Address',
        accessor: 'address',
    },
    {
        Header: 'Medical Record',
        accessor: 'medicalRecord',
    },
    {
        Header: 'Caregiver Id',
        accessor: 'caregiverId',
    },
    {
        Header: 'Caregiver Name',
        accessor: 'caregiverName',
    },
    {
        Header: 'Doctor Id',
        accessor: 'doctorId',
    },
    {
        Header: 'Doctor Name',
        accessor: 'doctorName',
    },
    {
        Header: 'Delete',
        accessor: 'deletePatient',
    },
    {
        Header: 'Update',
        accessor: 'updatePatient',
    },
    {
        Header: 'Medication Plan',
        accessor: 'medicationPlan',
    },
    {
        Header: 'View Medication Plan',
        accessor: 'viewMedicationPlan',
    }
];

const filters = [
    {
        accessor: 'Name',
    }
];

class PatientTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData

        };

    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />

        )
    }
}

export default PatientTable;
