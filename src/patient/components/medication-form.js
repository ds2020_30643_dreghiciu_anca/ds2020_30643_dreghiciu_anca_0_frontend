import React from 'react';
import validate from "./validators/patient-validator";
import Button from "react-bootstrap/Button";
import * as API_MEDICATION_PLAN from "../api/medicationPlan-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';

class MedicationPlanForm extends React.Component{

    constructor(props) {
            super(props);
            this.toggleFormMedication = this.toggleFormMedication.bind(this);
            this.reloadHandler = this.props.reloadHandler;
            this.getIdPatient = this.props.getIdPatient;

            this.state = {

                errorStatus: 0,
                error: null,

                formIsValid: false,

                formControls: {
                    intakeInterval: {
                        value: '',
                        placeholder: 'Intake Interval...',
                        valid: false,
                        touched: false,
                        validationRules: {
                            minLength: 3,
                            isRequired: true
                        }
                    },

                    startDate: {
                         value: '',
                         placeholder: 'Enter a start date...',
                         valid: false,
                         touched: false,

                    },
                    endDate: {
                        value: '',
                        placeholder: 'Enter a end date...',
                        valid: false,
                        touched: false

                    },

                    patientId: {
                        value: '',
                        placeholder: '1,2,3...',
                        valid: false,
                        touched: false,
                    },
                    medicationId: {
                        value: '',
                        placeholder: 'Medication Id...',
                        valid: false,
                        touched: false,
                    },
                    medicationName: {
                        value: '',
                        placeholder: 'Medication name..',
                        valid: false,
                        touched: false,
                    },
                    medicationSideEffects: {
                        value: '',
                        placeholder: 'Medication Side Effects...',
                        valid: false,
                        touched: false,
                    },
                    medicationDosage: {
                        value: '',
                        placeholder: 'Medication Dosage...',
                        valid: false,
                        touched: false,
                    },
                    medicationIntakeInterval: {
                        value: '',
                        placeholder: 'Medication Intake interval...',
                        valid: false,
                        touched: false,
                    },
                    planDosage: {
                        value: '',
                        placeholder: 'Plan Dosage...',
                        valid: false,
                        touched: false,
                    },



                }
            };

            this.handleChange = this.handleChange.bind(this);
            this.handleSubmit = this.handleSubmit.bind(this);
        }


    toggleFormMedication() {
            this.setState({collapseForm: !this.state.collapseForm});
        }

        handleChange = event => {

                const name = event.target.name;
                        const value = event.target.value;

                        const updatedControls = {
                            ...this.state.formControls
                        };

                        const updatedFormElement = {
                            ...updatedControls[name]
                        };

                        updatedFormElement.value = value;
                        updatedFormElement.touched = true;
                        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

                        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

                        updatedControls[name] = updatedFormElement;

                        let formIsValid = true;
                        for (let updatedFormElementName in updatedControls) {
                            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
                        }

                        this.setState({
                            formControls: updatedControls,
                            formIsValid: formIsValid
                        });


            };

    registerMedicationPlans(medicationPlan) {
            return API_MEDICATION_PLAN.postMedicationPlan(medicationPlan, (result, status, error) => {
                if (result !== null && (status === 200 || status === 201)) {
                    console.log("Successfully inserted patient with id: " + result);
                    this.reloadHandler();
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: error
                    }));
                }
            });
        }

    handleSubmit() {
             console.log("New medication Plan data:");
             console.log("Intake Interval: " + this.state.formControls.intakeInterval.value);
             console.log("startDate: " + this.state.formControls.startDate.value);
             console.log("endDate: " + this.state.formControls.endDate.value);
             console.log("patientId: " + this.state.formControls.patientId.value);
             console.log("Medication Id: " + this.state.formControls.medicationId.value);
             console.log("medication Name: " + this.state.formControls.medicationName.value);
             console.log("medication Side Effects: " + this.state.formControls.medicationSideEffects.value);
             console.log("medication Dosage: " + this.state.formControls.medicationDosage.value);
             console.log("medication Intake Interval: " + this.state.formControls.medicationIntakeInterval.value);
             console.log("medication Dosage: " + this.state.formControls.medicationDosage.value);

            let medicationPlan = {
                intakeInterval: this.state.formControls.intakeInterval.value,
                startDate: this.state.formControls.startDate.value,
                endDate: this.state.formControls.endDate.value,
                patientId: this.state.formControls.patientId.value,
                medicationToPlanDTOS:[
                    {
                        medicationDTO: {
                            id: this.state.formControls.medicationId.value,
                            name: this.state.formControls.medicationName.value,
                            sideEffects: this.state.formControls.medicationSideEffects.value,
                            dosage: this.state.formControls.medicationDosage.value
                        },
                        intakeInterval: this.state.formControls.medicationIntakeInterval.value,
                        dosage: this.state.formControls.planDosage.value
                    }
                ]
            };

            console.log(medicationPlan);
            this.registerMedicationPlans(medicationPlan);
        }

  render() {
          return (
              <div>

                  <FormGroup id='intakeInterval'>
                      <Label for='intakeIntervalField'> Intake Interval: </Label>
                      <Input name='intakeInterval' id='intakeIntervalField' placeholder={this.state.formControls.intakeInterval.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.intakeInterval.value}
                             touched={this.state.formControls.intakeInterval.touched? 1 : 0}
                             valid={this.state.formControls.intakeInterval.valid}
                             required
                      />
                      {this.state.formControls.intakeInterval.touched && !this.state.formControls.intakeInterval.valid &&
                      <div className={"error-message row"}> * intakeInterval must have at least 3 characters </div>}
                  </FormGroup>

                  <FormGroup id='startDate'>
                      <Label for='startDateField'> Start Date: </Label>
                      <Input name='startDate' id='startDateField' placeholder={this.state.formControls.startDate.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.startDate.value}
                             touched={this.state.formControls.startDate.touched? 1 : 0}
                             valid={this.state.formControls.startDate.valid}
                             required
                      />

                  </FormGroup>

                  <FormGroup id='endDate'>
                      <Label for='endDateField'> End Date: </Label>
                      <Input name='endDate'
                             placeholder={this.state.formControls.endDate.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.endDate.value}
                             touched={this.state.formControls.endDate.touched? 1 : 0}
                             valid={this.state.formControls.endDate.valid}
                             required
                      />

                  </FormGroup>

                  <FormGroup id='patientId'>
                      <Label for='patientIdField'> Patient Id: </Label>
                      <Input name='patientId'
                             placeholder={this.state.formControls.patientId.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.patientId.value}
                             touched={this.state.formControls.patientId.touched? 1 : 0}
                             valid={this.state.formControls.patientId.valid}
                             required
                      />
                  </FormGroup>

                  <FormGroup id='medicationId'>
                      <Label for='medicationIdField'> Medication Id: </Label>
                      <Input name='medicationId' id='medicationIdField'
                             placeholder={this.state.formControls.medicationId.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.medicationId.value}
                             touched={this.state.formControls.medicationId.touched? 1 : 0}
                             valid={this.state.formControls.medicationId.valid}
                             required
                      />
                  </FormGroup>
                  <FormGroup id='medicationName'>
                      <Label for='medicationNameField'> Medication Name: </Label>
                      <Input name='medicationName' id='medicationNameField' placeholder={this.state.formControls.medicationName.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.medicationName.value}
                             touched={this.state.formControls.medicationName.touched? 1 : 0}
                             valid={this.state.formControls.medicationName.valid}
                             required
                      />
                  </FormGroup>
                  <FormGroup id='medicationSideEffects'>
                      <Label for='medicationSideEffectsField'> Medication Side Effects: </Label>
                      <Input name='medicationSideEffects' id='medicationSideEffectsField' placeholder={this.state.formControls.medicationSideEffects.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.medicationSideEffects.value}
                             touched={this.state.formControls.medicationSideEffects.touched? 1 : 0}
                             valid={this.state.formControls.medicationSideEffects.valid}
                             required
                      />
                  </FormGroup>
                  <FormGroup id='medicationDosage'>
                      <Label for='medicationDosageField'> Medication Dosage: </Label>
                      <Input name='medicationDosage' id='medicationDosageField' placeholder={this.state.formControls.medicationDosage.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.medicationDosage.value}
                             touched={this.state.formControls.medicationDosage.touched? 1 : 0}
                             valid={this.state.formControls.medicationDosage.valid}
                             required
                      />
                  </FormGroup>
                  <FormGroup id='medicationIntakeInterval'>
                      <Label for='medicationIntakeIntervalField'> Medication Intake Interval: </Label>
                      <Input name='medicationIntakeInterval' id='medicationIntakeIntervalField' placeholder={this.state.formControls.medicationIntakeInterval.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.medicationIntakeInterval.value}
                             touched={this.state.formControls.medicationIntakeInterval.touched? 1 : 0}
                             valid={this.state.formControls.medicationIntakeInterval.valid}
                             required
                      />
                  </FormGroup>
                  <FormGroup id='planDosage'>
                      <Label for='planDosageField'> Plan Dosage: </Label>
                      <Input name='planDosage' id='planDosageField' placeholder={this.state.formControls.planDosage.placeholder}
                             onChange={this.handleChange}
                             defaultValue={this.state.formControls.planDosage.value}
                             touched={this.state.formControls.planDosage.touched? 1 : 0}
                             valid={this.state.formControls.planDosage.valid}
                             required
                      />
                  </FormGroup>


                      <Row>
                          <Col sm={{size: '4', offset: 8}}>
                              <Button type={"submit"} onClick={this.handleSubmit}>  Submit </Button>
                          </Col>
                      </Row>


                  {
                      this.state.errorStatus > 0 &&
                      <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                  }
              </div>
          ) ;
      }
  }

  export default MedicationPlanForm;
