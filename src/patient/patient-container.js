import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PatientForm from "./components/patient-form-add";
import PatientFormUpdate from "./components/patient-form-update";

import * as API_PATIENT from "./api/patient-api"
import * as API_MEDICATION from "./api/medicationPlan-api"
import PatientTable from "./components/patient-table";
import MedicationPlanForm from "./components/medication-form.js";
import MedicationPlanTable from "./components/medicationPlan-table";


class PatientContainer extends React.Component {

    constructor(props) {
        super(props);

        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);

        this.state = {
            selectedMedicationView:false,
            selectedMedicationPlan: false,
            selectedUpdate: false,
            selected: false,
            collapseForm: false,
            medicationDATA: [],
            tableMedication: [],
            tableData: [],
            isLoaded: false,
            isLoadedMedications: false,
            errorStatus: 0,
            error: null
        };
        this.handleDeleteButton = this.handleDeleteButton.bind(this);
        this.addButtons = this.addButtons.bind(this);
        this.updatePatient = this.updatePatient.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.handleMedicationPlan = this.handleMedicationPlan.bind(this);
        this.toggleFormMedication = this.toggleFormMedication.bind(this);
        this.viewMedicationPlan = this.viewMedicationPlan.bind(this);
        this.toggleMedicationPlanView = this.toggleMedicationPlanView.bind(this);



    }



    componentDidMount() {
        this.fetchPatients();

    }
    reloadDelete() {
        this.setState({
           isLoaded: false
           });
        this.fetchPatients();
    }



    handleDeleteButton = (idPatient) => {
        //console.log( "Id patient for delete: "+ idPatient);
        return API_PATIENT.deletePatient(idPatient, (result, status, err) =>{
            if(result !== null && status === 200) {
                this.reloadDelete();
            }
            else{
                this.setState({
                    errorStatus: status,
                    error: err
                })
            }

        });
    }

    updatePatient = (patient) =>
    {
        console.log("Id person for update: "+patient.id);
        this.setState({
            idPatient: patient.id
        })

        console.log("stare id: "+ this.idPatient);
        this.toggleFormUpdate(patient);
    }

    handleMedicationPlan = (patient) =>{
        this.setState({
            idPatient: patient.id
        })

        this.toggleFormMedication(patient);

    }


    viewMedicationPlan = (patient) =>{
            this.setState({
                idPatient: patient.id
            })
            console.log("patient id for medication Plan" + patient.id);
            this.fetchMedicationsPlan(patient.id);


        }


    addButtons = (data) => {
        const newData = data.map( patient => {
            return{
                ...patient,
                deletePatient:
                    <Button color = "danger" onClick={this.handleDeleteButton.bind(this, patient.id)}>
                        Delete
                    </Button>,
                updatePatient:
                    <Button color="info" onClick={this.updatePatient.bind(this, patient)}>
                        Update
                    </Button>,
                medicationPlan:
                    <Button color="info" onClick={this.handleMedicationPlan.bind(this, patient)}>
                        Create Plan
                    </Button>,
                viewMedicationPlan:
                    <Button color="info" onClick={this.viewMedicationPlan.bind(this, patient)}>
                         View Plan
                    </Button>

            }

        });
        return newData;
    }



    fetchMedicationsPlan(idPatient){
        return API_MEDICATION.getMedicationPlansById(idPatient, (result, status, err) => {
                if(result !== null && status === 200) {

                   this.setState({
                     tableMedication: result,
                     isLoaded: true
                   });
                   console.log(result);

                    this.toggleMedicationPlanView();


                }
                else
                    {
                    this.setState({
                        errorStatus: status,
                        error: err
                    });
                    }

                });
    }





    fetchPatients() {
        return API_PATIENT.getPatients((result, status, err) => {
        if(result !== null && status === 200) {
           let newTableData = this.addButtons(result);
           this.setState({
             tableData: newTableData,
             isLoaded: true
           })
           console.log("table Data "+ this.state.tableData);

        }
        else
            {
            this.setState({
                errorStatus: status,
                error: err
            });
            }

        });

    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleFormUpdate(idPatient)
    {
        console.log("id patient in toggle dar in container: "+ idPatient);
        this.setState({selectedUpdate: !this.state.selectedUpdate});

    }
    toggleFormMedication(idPatient)
    {
        console.log("id patient pt medication Plan" + idPatient);
        this.setState({selectedMedicationPlan: !this.state.selectedMedicationPlan});
    }
    toggleMedicationPlanView()
    {
        this.setState({selectedMedicationView: !this.state.selectedMedicationView});

    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPatients();

    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Patient Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Patient </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientTable tableData = {this.state.tableData}/>
                            }
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>


                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}> Update Patient: </ModalHeader>
                    <ModalBody>
                        <PatientFormUpdate reloadHandler={this.reload} patientId={this.state.idPatient}/>
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.selectedMedicationPlan} toggle={this.toggleFormMedication}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormMedication}> Medication Plan: </ModalHeader>
                    <ModalBody>
                        <MedicationPlanForm reloadHandler={this.reload} patientId={this.state.idPatient}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedMedicationView} toggle={this.toggleMedicationPlanView}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleMedicationPlanView}> Medication Plan: </ModalHeader>
                    <ModalBody>
                        <MedicationPlanTable  tableData = {this.state.tableMedication}/>
                    </ModalBody>
                </Modal>


            </div>

        )

    }
}


export default PatientContainer;
