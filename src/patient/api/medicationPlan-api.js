import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    medicationPlan: '/medicationPlan'
};

function getMedicationPlans(callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlan, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlansById(medicationPlanId, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + "/" + medicationPlanId , {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedicationPlan(user, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedicationPlans(medicationPlanId, callback){
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + "/" + medicationPlanId , {
        method: 'DELETE'
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function updateMedicationPlans(user, id, callback){
    console.log(user.id);
    let request = new Request(HOST.backend_api + endpoint.medicationPlan + "/" + id , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedicationPlans,
    getMedicationPlansById,
    postMedicationPlan,
    deleteMedicationPlans,
    updateMedicationPlans
};
